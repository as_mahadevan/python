print('Welcome to Shopmart')

username = input("Enter your username\t")
password = input("\nEnter your password\t")


def printItems(item_arr):
    print('\nS.No\tName\tPrice\n')
    index = 0
    for i in item_arr:
        print(index, '\t', i['item'], '\t', i['price'], '\n')
        index = index + 1


if (password == 'pass'):
    all_items = []
    try:
        with open('items.txt', 'r') as f:
            lines = f.readlines()
            for i in lines:
                i = i.replace('\n', '')
                str = i.split(',')
                all_items.append({
                    'id': int(str[0]),
                    'item': str[1],
                    'price': int(str[2])
                })
            f.close()
    except FileNotFoundError:
        print('\nYour cart items are empty')

    cart = []
    try:
        with open(username + '.txt', 'r') as f:
            lines = f.readlines()
            for i in lines:
                i = i.replace('\n', '')
                str = i.split(',')
                cart.append({
                    'id': int(str[0]),
                    'item': str[1],
                    'price': int(str[2])
                })
            f.close()
    except FileNotFoundError:
        print('\nYour cart items are empty')

    print(
        "\n1. List All Items\n2. Check your items in cart\n3. Add an Item to Cart\n4. Checkout"
    )

    choice = int(input("\nEnter your choice\t"))

    if (choice == 1):
        printItems(all_items)
    elif (choice == 2):
        if len(cart) == 0:
            print('\nYour cart is empty')
        else:
            for item in cart:
                print(item['item'], "\n")
    elif (choice == 3):
        item_no = int(input('\nEnter an item no to add\t'))
        for i in all_items:
            if i['id'] == item_no:
                with open(username + ".txt", 'a') as file:
                    list = [
                        str(i['id']) + ',',
                        str(i['item']) + ',',
                        str(i['price']) + '\n'
                    ]
                    file.writelines(list)

    elif (choice == 4):
        exit()

else:
    exit()