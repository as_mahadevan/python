print('Welcome to Shopmart')

from dbclass import Database

isAuthenticated = False

database = Database()


def checkIfAuthenticated(username, password):
    str = "SELECT * from user WHERE username='{username}'".format(
        username=username)
    result = database.run_query(str)
    if (len(result) == 0):
        isAuthenticated = False
    else:
        for i in result:
            if i[0] == username:
                if i[1] != password:
                    isAuthenticated = False
                else:
                    isAuthenticated = True
    if (isAuthenticated):
        return True
    else:
        return False


username = input("Enter your username\t")
password = input("\nEnter your password\t")


def printItems():
    allItems = database.run_query('SELECT * FROM products')
    products = []
    for i in allItems:
        products.append({'id': int(i[0]), 'item': i[1], 'price': int(i[2])})
    if len(products) == 0:
        print('\nYour cart is empty')
    else:
        for item in products:
            print(item['item'], "\n")


def getProducts():
    allItems = database.run_query('SELECT * FROM products')
    products = []
    for i in allItems:
        products.append({'id': int(i[0]), 'item': i[1], 'price': int(i[2])})
    return products


def printcartitems():
    cart = []
    cartitems = database.run_query('SELECT * FROM cart')
    for i in cartitems:
        cart.append({'id': int(i[1]), 'item': i[2], 'price': int(i[3])})
    if len(cart) == 0:
        print('\nYour cart is empty')
    else:
        for item in cart:
            print(item['item'], "\n")


if (checkIfAuthenticated(username, password)):
    all_items = []
    print('Cart items:')
    printcartitems()

    print(
        "\n1. List All Items\n2. Check your items in cart\n3. Add an Item to Cart\n4. Checkout"
    )

    choice = int(input("\nEnter your choice\t"))

    if (choice == 1):
        printItems()
    elif (choice == 2):
        printcartitems()
    elif (choice == 3):
        item_no = int(input('\nEnter an item no to add\t'))
        products = getProducts()
        for i in products:
            if i['id'] == item_no:
                str = "Insert into cart (productid, name, price) values ({productid}, '{productname}', {productprice})".format(
                    productid=item_no,
                    productname=i['item'],
                    productprice=i['price'])
                print(str)
                result = database.run_query(str)

    elif (choice == 4):
        exit()

else:
    exit()